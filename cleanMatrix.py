__author__ = 'mostafamohsenvand'
import sys
import json
print "="*100
###################################################################################################
############################ Pre-processing and grouping the data #################################
###################################################################################################
sys.stdout.write("Importing the JSON file . . .")
TagLists = [] # a list of lists of tags
finalTagList =[] # a flat list of all unique tags

#openning the raw json data (run csv2dict first on the raw csv data to get the json file)
with open('mathdata.json') as jsf:
    data = json.loads(jsf.read())
    for content in data.values():
        #checking for tags that contribute at least in a single edge
        if len(content["Tags"]) >1:
            #fetch a single tag coocurance list
            postTags = content["Tags"]
            # appending the tag list of a single question to the list of taglists
            TagLists.append(postTags)
            # appending the elements (not nested) to the finalTagList
            finalTagList += postTags
# List of exceptional tags that should be taken out. Can be left empty
# exceptionList = set()
exceptionList = set(['homework','reference-request','soft-question','gmat-exam'])
# remove duplicates and exceptions
finalTagList = list(set(finalTagList)-exceptionList)

sys.stdout.write(" done\n")
# calculating the number of all tags and all posts
numPost = len(TagLists)
numTags = len(finalTagList)

print "number of unique tags is {0} in {1} posts".format(numTags, numPost)
print "="*100

sys.stdout.write("Saving the list of all tags . . .")

with open('mathTagList.txt','wb') as tlf:
    for tag in finalTagList:
        tlf.write(tag+'\n')
sys.stdout.write(" done\n")

print '='*100

sys.stdout.write("constructing the adjacency matrix . . .")

###################################################################################################
###################################### the adjacency matrix #######################################
###################################################################################################


#the empty dictionary for the matrix
theMatrix = {}
#create a node for each tag
for tag in finalTagList:
    theMatrix[tag]={}
#traverse through the TagLists and pick the edges and add them to the matrix
#and form a dict with a structure as shown here:
        ##           node_i: {
        ##               node_1: co_occurrence_of_nodes(i,1),
        ##               node_2: co_occurrence_of_nodes(i,2),
        ##               ...
        ##               node_j: co_occurrence_of_nodes(i,j),
        ##               ...
        ##                   }

for tagrow in TagLists:
    for tag in tagrow:
        if theMatrix.has_key(tag):
            for otherTag in (set(tagrow)-{tag}):
                if theMatrix.has_key(otherTag):
                    if theMatrix[tag].has_key(otherTag):
                        theMatrix[tag][otherTag]+=1
                    else:
                        theMatrix[tag][otherTag]=1
sys.stdout.write(" done\n")

# saving the matrix for further uses
sys.stdout.write("Saving the adjacency matrix . . .")
with open('mathCleanMatrix.json','wb') as jsonFile:
    jsonFile.write(json.dumps(theMatrix, indent=2, sort_keys=False))
sys.stdout.write(" done\n")
print "="*100


###################################################################################################
####################################### the weighted graph ########################################
###################################################################################################

sys.stdout.write("constructing the Graph . . .")

#using the package NetworkX

import networkx as nx
import igraph as ig
# create a new graph
G = nx.Graph()
# use tags to add and label all the nodes
G.add_nodes_from(finalTagList)

#initialize the edge list
edgeList=[]

# add the edges and their weights to the edge list
for vertex in finalTagList:
    for otherVertex in theMatrix[vertex].keys():
        # the edge list should be a list of tuples with 3 entries, (node1, node2, weight)
        edgeList.append((vertex,otherVertex,theMatrix[vertex][otherVertex]))

#use the method of add_weighted_edges_from that takes a list of tuples as shown before
G.add_weighted_edges_from(edgeList)

sys.stdout.write(" done\n")

#save a copy of the labeled and weighted graph in GML format for further visualizations
sys.stdout.write("Saving the Graph as GML . . .")
nx.write_gml(G,"MathGraph.gml")

#construct the iGraph instance using the GML file
sys.stdout.write(" done\n")

sys.stdout.write("Loading the graph as iGraph instance...")

#construct the iGraph instance using the GML file
gr = ig.read("MathGraph.gml")
sys.stdout.write(" done\n")
print "="*100

###################################################################################################
####################################### Centrality Analysis #######################################
###################################################################################################

print "Analysis :"

#using NetworkX for centrality analysis of the graph

sys.stdout.write("calculating Degrees . . .")
DegreeDict = nx.degree(G)
sys.stdout.write(" done\n")

sys.stdout.write("calculating PageRank Centrality . . .")
PageRankDict = nx.pagerank(G)
sys.stdout.write(" done\n")

sys.stdout.write("calculating Closeness Centrality . . .")
ClosenessDict = nx.closeness_centrality(G)
sys.stdout.write(" done\n")

sys.stdout.write("calculating Betweenness Centrality . . .")
BetweennessDict = nx.betweenness_centrality(G)
sys.stdout.write(" done\n")

sys.stdout.write("calculating Communicability Centrality . . .")
CommunicabilityDict= nx.communicability_centrality(G)
sys.stdout.write(" done\n")

print "="*100

###################################################################################################
#################################### Hierarchical Clustering ######################################
###################################################################################################


sys.stdout.write("Clustering by depth 3 . . .")

# using community library to perform community detection.
# check this address for documentation and installation of the package:
# http://perso.crans.org/aynaud/communities/
# note that it won't install using pip and the user should use setup.py to add it to the virtualenv.

import community

#tagPath is going to store the cluster path. A cluster path that is assigned to each node(tag) is an address like /0/1/2
# the address shows the hierarchical classification of the node or it's address in the ontology tree
# / : root /0 : the first layer /0/1 : the second layer ...
tagPath = {}
for tag in finalTagList :
    #starting with the root of the tree
    tagPath[tag] = "/"

# first community detection:
partition0 = community.best_partition(G)
# number of communities in the first layer
maxval0 = max(partition0.values())

# adding the first layer address to the tagPath
for tag in partition0:
    tagPath[tag] += str(partition0[tag])
#starting the hierarchical clustering:
# 1-take each cluster
for i in range(maxval0+1):
    # create a sub-graph for the cluster
    G1 = nx.Graph()
    # choose the right nodes (clusters are numbered from 0 to the number of clusters -1)
    # here "i" notes the number of cluster
    tagListG1 = [tag for tag in partition0.keys() if partition0[tag]==i]
    # add nodes
    G1.add_nodes_from(tagListG1)
    # add edges as before
    edgeListG1=[]
    for vertex in tagListG1:
        for otherVertex in theMatrix[vertex].keys():
            if otherVertex in tagListG1:
                edgeListG1.append((vertex,otherVertex,theMatrix[vertex][otherVertex]))
    G1.add_weighted_edges_from (edgeListG1)
    # sub-clustering
    partitionG1 = community.best_partition(G1)
    # updating the paths by adding the second layer address
    for tag in partitionG1:
        tagPath[tag] += "/" + str(partitionG1[tag])
    # number of subclusters
    maxval1 = max(partitionG1.values())
    # repeat the same process again !
    for j in range(maxval1+1):
        G2 = nx.Graph()
        tagListG2 = [tag for tag in partitionG1.keys() if partitionG1[tag]==j]
        G2.add_nodes_from(tagListG2)
        edgeListG2=[]
        for vertex in tagListG2:
            for otherVertex in theMatrix[vertex].keys():
                if otherVertex in tagListG2:
                    edgeListG2.append((vertex,otherVertex,theMatrix[vertex][otherVertex]))
        G2.add_weighted_edges_from (edgeListG2)
        partitionG2 = community.best_partition(G2)
        for tag in partitionG2:
            tagPath[tag] += "/"+str(partitionG2[tag])
sys.stdout.write(" done\n")
print "="*100

###################################################################################################
####################################### Saving the results ########################################
###################################################################################################

print "Saving the Analysis results as CSV."
with open('AnalysisResults.csv','wb') as csvo:
    csvo.write('Tag,cluster,Degree,PageRank,Closeness,Betweenness,Communicability'+'\n')
    for tag in DegreeDict:
        csvo.write(tag+','+tagPath[tag]+','+str(DegreeDict[tag])+','+str(PageRankDict[tag])+','
                   +str(ClosenessDict[tag])+','+str(BetweennessDict[tag])+','+str(CommunicabilityDict[tag])+'\n')


print "Saving the Analysis results as CSV - Separate Path."
with open('AnalysisResults_Sep.csv','wb') as csvo:
    csvo.write('Tag,l1,l2,l3,Degree,PageRank,Closeness,Betweenness,Communicability'+'\n')
    for tag in DegreeDict:
        address = tagPath[tag][1:].split('/')
        csvo.write(tag+','+str(address[0])+','+str(address[1])+','+str(address[2])+','+str(DegreeDict[tag])+','+str(PageRankDict[tag])+','
                   +str(ClosenessDict[tag])+','+str(BetweennessDict[tag])+','+str(CommunicabilityDict[tag])+'\n')


print "Saving the Analysis results as JSON."

resultsDict ={}
for tag in finalTagList:
    address = tagPath[tag][1:].split('/')
    resultsDict[tag] = {'l1':address[0],
                        'l2':address[1],
                        'l3':address[2],
                        'Degree':DegreeDict[tag],
                        'PageRank':PageRankDict[tag],
                        'Closeness':ClosenessDict[tag],
                        'Betweenness':BetweennessDict[tag],
                        "Communicability":CommunicabilityDict[tag]
                        }
with open('AnalysisResults.json','wb') as jsfA:
    jsfA.write(json.dumps(resultsDict, indent = 4))