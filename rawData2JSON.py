__author__ = 'MinaSoltan'
import csv
import json
import copy #to create a deep copy

# load the raw csv file, the file is downloaded 
input_file = csv.DictReader(open("mathRaw.csv"))
# empty dictionary
csvHash = {}

# create a dic with keys : id of the posts and value: {Tags,CreationDate,Title}
for row in input_file:
    #obtain a copy of the row
    rowCopy = copy.deepcopy(row)
    # take the id key out
    rowCopy.pop('id')
    # break the Tags string into a list of tags "<a><b><c>" --> ["a", "b", "c"]
    tags = rowCopy['Tags'][1:-1].split('><')
    rowCopy['Tags'] = tags
    #construct the dictionart
    csvHash[row['id']]=rowCopy
#write the dict into a JSON file
with open('mathdata.json','wb') as jsonFile:
    jsonFile.write(json.dumps(csvHash, indent=2))
