import json
analysisDict = {}
trendsDict = {}

with open('AnalysisResults.json') as ajson:
    analysisDict = json.loads(ajson.read())

with open('mathTrends.json') as tjson:
    trendsDict = json.loads(tjson.read())

def pathMatch(tag,path):
    lp = len(path)
    if lp == 0 : return True
    elif lp == 1: return [int(analysisDict[tag]["l1"])] == path
    elif lp == 2: return [int(analysisDict[tag]["l1"]), int(analysisDict[tag]["l2"])] == path
    elif lp == 3: return [int(analysisDict[tag]["l1"]), int(analysisDict[tag]["l2"]), int(analysisDict[tag]["l3"])] == path

def pathNextLevelDepth(path):
    depth = 0
    lp = len(path)
    for tag in analysisDict:
        if pathMatch(tag,path):
            if lp == 0:
                possibleDepth = int(analysisDict[tag]["l1"])
                if depth<possibleDepth: depth = possibleDepth
            elif lp == 1:
                possibleDepth = int(analysisDict[tag]["l2"])
                if depth<possibleDepth: depth = possibleDepth
            elif lp == 2:
                possibleDepth = int(analysisDict[tag]["l3"])
                if depth<possibleDepth: depth = possibleDepth
    return depth

def header(path,centrality):
    out ={}
    for i in range(pathNextLevelDepth(path)+1):
        newPath = path + [i]
        matchedSubCluster=[]
        for tag,values in analysisDict.items():
            if pathMatch(tag,newPath):
                matchedSubCluster.append([values[centrality],tag])
        matchedSubCluster.sort(key=lambda x: -x[0])
        topList = matchedSubCluster[:min(3,len(matchedSubCluster))]
        out[i]= "|".join([x[1] for x in topList])
    return out

def Tagger(path,centrality):
    matchedSubCluster=[]
    for tag,values in analysisDict.items():
        if pathMatch(tag,path):
            matchedSubCluster.append([values[centrality],tag])
    matchedSubCluster.sort(key=lambda x: -x[0])
    topList = matchedSubCluster[:min(3,len(matchedSubCluster))]
    out = " | ".join([x[1] for x in topList])
    return out

def pathDictFullVolume(path,centrality):
    out={}
    guide = ["l1","l2","l3"]
    pathHeader = header(path,centrality)
    for date in trendsDict:
        for tag in trendsDict[date]:
            out[date]={}
            if pathMatch(tag,path):
                finerPath = analysisDict[tag][guide[len(path)]]
                newTag = pathHeader[int(finerPath)]
                if out[date].has_key(newTag):
                    out[date][newTag]+=trendsDict[date][tag]
                else :
                    out[date][newTag]=trendsDict[date][tag]

    return out

def pathDictFullVolume(path,centrality):
    out={}
    guide = ["l1","l2","l3"]
    pathHeader = header(path,centrality)
    for date in trendsDict:
        for tag in trendsDict[date]:
            out[date]={}
            if pathMatch(tag,path):
                finerPath = analysisDict[tag][guide[len(path)]]
                newTag = pathHeader[int(finerPath)]
                if out[date].has_key(newTag):
                    out[date][newTag]+=trendsDict[date][tag]
                else :
                    out[date][newTag]=trendsDict[date][tag]
    return out

pathDictFV = pathDictFullVolume([0],"PageRank")

outHeader = header([0],"PageRank")
tagList = [outHeader[i] for i in range(len(outHeader))]

with open('TrendsForPath_0.csv','wb') as csvFile:
    preheading = ",".join([" | ".join([w.capitalize() for w in s.split(" | ")]) for s in tagList])
    heading = 'Date,'+preheading+"\n"
    csvFile.write(heading)
    for tagDay in pathDictFV:
        volumes ={}
        for tag in tagList:
            if pathDictFV[tagDay].has_key(tag):
                volumes[tag] = pathDictFV[tagDay][tag]
            else : volumes[tag] = 0
        volumesList = [volumes[tagname] for tagname in tagList]
        row = ",".join(["{"+str(i)+"}"for i in range(len(tagList)+1)]) + '\n'
        csvFile.write(row.format(tagDay,*volumesList))