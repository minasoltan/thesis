
--  MinaSoltan
--  run the query at : http://data.stackexchange.com/math/query/new  to query mathematics database
Select id,Tags,Title,CreationDate from Posts where LEN(Tags) <> 0 
--  note that it only takes the posts with tags.

-- in case all the data doesn't fit within 50,000 rows, then give it a starting and ending date like this
Select id,Tags,Title,CreationDate from Posts where LEN(Tags) <> 0 AND CreationDate Between '2010-08-01' AND '2010-09-01' 

-- the score field gives the number of up-votes.
Select id,Tags,Title,CreationDate from Posts where LEN(Tags) <> 0 AND CreationDate Between '2010-08-01' AND '2010-09-01' AND Score >1