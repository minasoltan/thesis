__author__ = 'MinaSoltan '
import json
data = {}
with open('mathdata.json') as jsf:
    data = json.loads(jsf.read())

tagList = []
tagListDict = {}
with open('mathTagList.txt') as tagFile:
    tagList = tagFile.read().split()
for tag in tagList:
    tagListDict[tag]=None
# targetTrendDict = {}
# for post in data:
#     for tag in tagList:
#         if tag in data[post]['Tags']:
#             tagDay = data[post]['CreationDate'].split()[0]
#             if targetTrendDict.has_key(tagDay):
#                 if targetTrendDict[tagDay].has_key(tag):
#                     targetTrendDict[tagDay][tag] += 1
#                 else:
#                     targetTrendDict[tagDay][tag] = 1
#             else:
#                 targetTrendDict[tagDay] = {}

targetTrendDict = {}
for post in data:
    for tag in data[post]['Tags']:
        if tagListDict.has_key(tag):
            tagDay = data[post]['CreationDate'].split()[0]
            if targetTrendDict.has_key(tagDay):
                if targetTrendDict[tagDay].has_key(tag):
                    targetTrendDict[tagDay][tag] += 1
                else:
                    targetTrendDict[tagDay][tag] = 1
            else:
                targetTrendDict[tagDay] = {}


with open('mathTrends.json','wb') as results:
    results.write(json.dumps(targetTrendDict))

with open('mathTrends.csv','wb') as csvFile:
    header = 'Date,'+','.join([s.capitalize() for s in tagList])+"\n"
    csvFile.write(header)
    for tagDay in targetTrendDict:
        volumes ={}
        for tag in tagList:
            if targetTrendDict[tagDay].has_key(tag):
                volumes[tag] = targetTrendDict[tagDay][tag]
            else : volumes[tag] = 0
        volumesList = [volumes[tagname] for tagname in tagList]
        row = ",".join(["{"+str(i)+"}"for i in range(len(tagList)+1)]) + '\n'
        csvFile.write(row.format(tagDay,*volumesList))