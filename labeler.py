import json
import copy
analysisDict = {}
with open('AnalysisResults.json') as anaJsf:
    analysisDict = json.loads(anaJsf.read())

dictWithHeaders = copy.deepcopy(analysisDict)

def pathMatch(tag,path):
    lp = len(path)
    if lp == 0 : return True
    elif lp == 1: return [int(analysisDict[tag]["l1"])] == path
    elif lp == 2: return [int(analysisDict[tag]["l1"]), int(analysisDict[tag]["l2"])] == path
    elif lp == 3: return [int(analysisDict[tag]["l1"]), int(analysisDict[tag]["l2"]), int(analysisDict[tag]["l3"])] == path

def pathNextLevelDepth(path):
    depth = 0
    lp = len(path)
    for tag in analysisDict:
        if pathMatch(tag,path):
            if lp == 0:
                possibleDepth = int(analysisDict[tag]["l1"])
                if depth<possibleDepth: depth = possibleDepth
            elif lp == 1:
                possibleDepth = int(analysisDict[tag]["l2"])
                if depth<possibleDepth: depth = possibleDepth
            elif lp == 2:
                possibleDepth = int(analysisDict[tag]["l3"])
                if depth<possibleDepth: depth = possibleDepth
    return depth

def header(path,centrality):
    out ={}
    for i in range(pathNextLevelDepth(path)+1):
        newPath = path + [i]
        matchedSubCluster=[]
        for tag,values in analysisDict.items():
            if pathMatch(tag,newPath):
                matchedSubCluster.append([values[centrality],tag])
        matchedSubCluster.sort(key=lambda x: -x[0])
        topList = matchedSubCluster[:min(3,len(matchedSubCluster))]
        out[i]= "|".join([x[1] for x in topList])
    return out

for tag, values in analysisDict.items():
    dictWithHeaders[tag]['l1'] = header([],"PageRank")[int(values['l1'])]
    dictWithHeaders[tag]['l2'] = header([int(values['l1'])],"PageRank")[int(values['l2'])]
    dictWithHeaders[tag]['l3'] = header([int(values['l1']),int(values['l2'])],"PageRank")[int(values['l3'])]

    # dictWithHeaders[tag]['l1'] = header([],"PageRank")[int(values['l1'])]
    # dictWithHeaders[tag]['l2'] = header([int(values['l1'])],"PageRank")[int(values['l2'])]
    # dictWithHeaders[tag]['l3'] = header([int(values['l1']),int(values['l2'])],"PageRank")[int(values['l3'])]
    #
    # dictWithHeaders[tag]['l1'] = header([],"PageRank")[int(values['l1'])]
    # dictWithHeaders[tag]['l2'] = header([int(values['l1'])],"PageRank")[int(values['l2'])]
    # dictWithHeaders[tag]['l3'] = header([int(values['l1']),int(values['l2'])],"PageRank")[int(values['l3'])]

print "Saving the Analysis results as CSV - Separate Path."
with open('AnalysisResults_Sep_label.csv','wb') as csvo:
    csvo.write('Tag,l1,l2,l3,Degree,PageRank,Closeness,Betweenness,Communicability'+'\n')
    for tag,values in dictWithHeaders.items():
        address = [values['l1'],values['l2'],values['l3']]
        csvo.write(tag+','+str(address[0])+','+str(address[1])+','+str(address[2])+','+str(values['Degree'])+','+str(values['PageRank'])+','
                   +str(values['Closeness'])+','+str(values['Betweenness'])+','+str(values['Communicability'])+'\n')
